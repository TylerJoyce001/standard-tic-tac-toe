# Aesthetic preference to delay messages displayed
from time import sleep

# Function defining entries on board
def print_board(entries):
    # Variables for board, new play on board, and how to restructure the board
    line = "+---+---+---+"
    output = line
    n = 0

    # Restructures board as moves are made, adding | depending on board number 1 - 9
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

# Function to delay Game over message... to add Suspense!
def suspense():
    sleep(0.5)
    print(".")
    sleep(0.5)
    print(".")
    sleep(0.5)
    print(".")
    sleep(0.5)
    print("Game Over")
    sleep(1.5)


def is_all_same(lst):
    return len(set(lst)) == 1

# Create function to simplify code
# Two parameters winner and win message
# Accepts two arguments later: current_player and message
# Display final board, display message and exit
def game_over(winner):
    print(winner, "Has won")
    suspense()
    exit()

#Tyler Made a Edit

# Checks if top middle or bottom rows hold same value
def is_row_winner(board):
    row1 = board[0:3]
    row2 = board[3:6]
    row3 = board[6:9]

    for row in [row1, row2, row3]:
        if is_all_same(row):
            return True
    return False

def is_c_winner(board):
    col1 = [board[0], board[3], board[6]]
    col2 = [board[1], board[4], board[7]]
    col3 = [board[2], board[5], board[8]]

    for col in [col1, col2, col3]:
        if is_all_same(col):
            return True
    return False


# Given a list and a variable number of integers, determines if the value at all given indecies are the same.
def all_index_same(lst, *indecies):
    val = lst[indecies[0]]
    for index in indecies:
        if lst[index] != val:
            return False
    return True

# If top left to bottom right diagnal holds same value: game over
def left_diagonal(board):
    return all_index_same(board, 0, 4, 8)

# If bottom left to top right holds same value: game over
def right_diagonal(board):
    return all_index_same(board, 2, 4, 6)


# Variables for plays/board spaces and player
board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

# Check if there is a winner in any direction.
def has_winner(board):
    return is_row_winner(board) or is_c_winner(board) or left_diagonal(board) or right_diagonal(board)


# Function asking where players want to move
for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if has_winner(board):
        game_over(current_player)

    # Switches player every other time
    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
sleep(1.5)
